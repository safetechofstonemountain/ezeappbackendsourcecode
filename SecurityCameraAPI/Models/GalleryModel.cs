﻿using SecurityCameraAPI.Helpers;

namespace SecurityCameraAPI.Models
{
    public class GalleryModel
    {
        public int Id { get; set; }         
        public int UserId { get; set; }      
        public string Email { get; set; }
        public string ImageURL { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string? Address { get; set; }
        public DateTime ImageDateTime { get; set; }
        public bool IsFavourite { get; set; }
        public IFormFile Files { get; set; }
    }
    public class ReturnGalleryModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        public string ImageURL { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string? Address { get; set; }
        public DateTime ImageDateTime { get; set; }
        public bool IsFavourite { get; set; }
    }
    public class ReturnGalleryData
    {
        public int Status_Code { get; set; }
        public string Message { get; set; }
        public IEnumerable<ReturnGalleryModel> Gallery_Detail { get; set; }

    }
    public class EditGalleryModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public bool IsFavourite { get; set; }
    }
}
