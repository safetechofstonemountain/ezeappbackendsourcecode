﻿namespace SecurityCameraAPI.Models
{
    public class ForgotPinModel
    {
        public string Email { get; set; }
    }
}
