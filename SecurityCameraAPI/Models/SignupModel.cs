﻿using System.ComponentModel.DataAnnotations;

namespace SecurityCameraAPI.Models
{
    public class SignupModel
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        
        [EmailAddress]
        public string Email { get; set; }
        public string Password { get; set; }
        public int SecurityPin { get; set; }
    }
}
