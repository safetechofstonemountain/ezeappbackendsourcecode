﻿namespace SecurityCameraAPI.Models
{
    public class SettingModel
    {
        public string PrivacyPolicy { get; set; }
        public string TermsNCondition { get; set; }
    }
    public class ReturnSettingData
    {
        public int Status_Code { get; set; }
        public string Message { get; set; }
        public SettingModel settingModel { get; set; }

    }
}
