﻿using System.ComponentModel.DataAnnotations;

namespace SecurityCameraAPI.Models
{
    public class SigninModel
    {
        [EmailAddress]
        public string Email { get; set; }
        public int SecurityPin { get; set; }
    }
    public class SigninPinModel
    {
        public int SecurityPin { get; set; }
    }
    public class LogoutModel
    {
        public string Token { get; set; }
    }
}
