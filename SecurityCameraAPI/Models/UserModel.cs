﻿using System.ComponentModel.DataAnnotations;
namespace SecurityCameraAPI.Models
{
    public class UserModel
    {
        public int Id { get; set; }        
        public string Name { get; set; }        
        public string LastName { get; set; }        
        [EmailAddress]
        public string Email { get; set; }        
        public string Password { get; set; }        
        public int SecurityPin { get; set; }
        public string Token { get; set; }
        public DateTime? TokenExpires { get; set; }        
    }
   
    public class ReturnData
    {        
        public int Status_Code { get; set; }
        public string Message { get; set; }
        public UserModel User_Detail { get; set; }
        
    }
    public class BlankDictionary
    {

    }
    public class ReturnResponse
    {
        public int Status_Code { get; set; }
        public string Message { get; set; }
        public BlankDictionary Blank_Details { get; set; }
    }
}
