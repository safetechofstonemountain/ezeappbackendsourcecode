﻿namespace SecurityCameraAPI.Models
{
    public class NotificationModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string NotificationStatus { get; set; }
        public DateTime StatusDateTime { get; set; }
        public bool IsRead { get; set; }
    }
    public class ReturnNotificationData
    {
        public int Status_Code { get; set; }
        public string Message { get; set; }
        public IEnumerable<NotificationModel> Notification_Detail { get; set; }

    }
    public class EditNotificationModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public bool IsRead { get; set; }
    }
}
