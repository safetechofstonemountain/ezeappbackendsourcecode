﻿namespace SecurityCameraAPI.Models
{
    public class ChangePinModel
    {
        public string Email { get; set; }
        public int OldSecurityPin { get; set; }
        public int NewSecurityPin { get; set; }
    }   
}
