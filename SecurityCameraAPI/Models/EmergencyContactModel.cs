﻿namespace SecurityCameraAPI.Models
{
    public class EmergencyContactModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Relationship { get; set; }
        public string ContactNumber { get; set; }
    }  

    public class ReturnContactData
    {
        public int Status_Code { get; set; }
        public string Message { get; set; }
        public IEnumerable<EmergencyContactModel> Contact_Detail { get; set; }

    }
}
