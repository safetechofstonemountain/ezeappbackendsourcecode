using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using SecurityCameraAPI.Data;
using SecurityCameraAPI.Helpers;
using SecurityCameraAPI.IRepository;
using SecurityCameraAPI.Repository;
using SecurityCameraAPI.Settings;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

//Add DbContext
builder.Services.AddDbContext<SecurityCameraContext>(
    options => options.UseSqlServer(builder.Configuration.GetConnectionString("SecurityCameraDB")));

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddTransient<IJsonAttribute, FromJsonAttribute>();
builder.Services.AddTransient<IUserRepository, UserRepository>();
builder.Services.AddTransient<IGalleryRepository, GalleryRepository>();
builder.Services.AddTransient<IEmergencyContactRepository, EmergencyContactRepository>();
builder.Services.AddTransient<ISettingRepository, SettingRepository>();

builder.Services.AddAutoMapper(typeof(Program));

builder.Services.AddCors(option =>
{
    option.AddDefaultPolicy(builder =>
    {
        builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
    });
});

builder.Services.AddCors(c =>
{
    c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
});

var connSettingsSection = builder.Configuration.GetSection("ConnectionSettings");
builder.Services.Configure<ConnectionSettings>(connSettingsSection);

var connSettings=connSettingsSection.Get<ConnectionSettings>();

var appSettingsSection = builder.Configuration.GetSection("AppSettings");
builder.Services.Configure<AppSettings>(appSettingsSection);

//configure jwt authentication
var appSettings = appSettingsSection.Get<AppSettings>();
var key=Encoding.ASCII.GetBytes(appSettings.Secret);
//var key = Encoding.ASCII.GetBytes(builder.Configuration["AppSettings:Secret"]);

builder.Services.AddAuthentication(option =>
{
    option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    option.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;

})
    .AddJwtBearer(option=>
    {
        option.SaveToken = true;
        option.RequireHttpsMetadata=false;
        option.TokenValidationParameters = new TokenValidationParameters()
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(key),
            ValidateIssuer = false,
            ValidateAudience = false           
            
        };
    });

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddMvc(properties =>
{
    properties.ModelBinderProviders.Insert(0, new JsonModelBinderProvider());
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment() || app.Environment.IsProduction())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        string swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
        c.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", "Security Camera API");
    });
}

app.UseFileServer(new FileServerOptions
{
    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "ImageUpload")),
    RequestPath = "/ImageUpload",
    EnableDefaultFiles = true
});

app.UseHttpsRedirection();


app.UseRouting();
app.UseCors();
app.UseCors(options => options.AllowAnyOrigin());
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
