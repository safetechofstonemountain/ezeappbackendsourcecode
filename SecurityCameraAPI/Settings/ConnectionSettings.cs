﻿namespace SecurityCameraAPI.Settings
{
    public class ConnectionSettings
    {
        public string ConnectionString { get; set; }
    }
}
