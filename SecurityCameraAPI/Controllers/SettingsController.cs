﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SecurityCameraAPI.IRepository;
using SecurityCameraAPI.Models;
using SecurityCameraAPI.Settings;

namespace SecurityCameraAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SettingsController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly ISettingRepository _settingRepository;
        public SettingsController(IUserRepository userRepository,ISettingRepository settingRepository)
        {
            _userRepository = userRepository;
            _settingRepository = settingRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetPrivacyPolicy()
        {
            ReturnSettingData returnSettingData = new ReturnSettingData();
            returnSettingData.settingModel = new SettingModel();

            returnSettingData.Status_Code= Convert.ToInt32(Ok().StatusCode);
            returnSettingData.Message = "Privacy Policy/Terms & Condition URL";

            returnSettingData.settingModel.PrivacyPolicy = "<h1>Privacy Policy </h1>This privacy policy will help you understand how[name] uses and protects the data you provide to us when you visit and use[website] ('website', 'service').We reserve the right to change this policy at any given time, of which you will be promptly updated.If you want to make sure that you are up to date with the latest changes, we advise you to frequently visit this page.<h3> What User Data We Collect</h3>When you visit the website, we may collect the following data:•\tYour IP address.•\tYour contact information and email address.•\tOther information such as interests and preferences. •\tData profile regarding your online behavior on our website.<h3> Why We Collect Your Data</h3>We are collecting your data for several reasons:•\tTo better understand your needs.•\tTo improve our services and products. •\tTo send you promotional emails containing the information we think you will find interesting.•\tTo contact you to fill out surveys and participate in other types of market research.•\tTo customize our website according to your online behavior and personal preferences.<h3> Safeguarding and Securing the Data </h3>[name] is committed to securing your data and keeping it confidential. [name] has done all in its power to prevent data theft, unauthorized access, and disclosure by implementing the latest technologies and software, which help us safeguard all the information we collect online.<h3> Our Cookie Policy </h3>Once you agree to allow our website to use cookies, you also agree to use the data it collects regarding your online behavior(analyze web traffic, web pages you spend the most time on, and websites you visit)The data we collect by using cookies is used to customize our website to your needs. After we use the data for statistical analysis, the data is completely removed from our systems.Please note that cookies don't allow us to gain control of your computer in any way. They are strictly used to monitor which pages you find useful and which you do not so that we can provide a better experience for you.If you want to disable cookies, you can do it by accessing the settings of your internet browser. You can visit www.internetcookies.com, which contains comprehensive information on how to do this on a wide variety of browsers and devices. <h3> Links to Other Websites </h3>Our website contains links that lead to other websites. If you click on these links[name] is not held responsible for your data and privacy protection.Visiting those websites is not governed by this privacy policy agreement.Make sure to read the privacy policy documentation of the website you go to from our website.<h3> Restricting the Collection of your Personal Data </h3>At some point, you might wish to restrict the use and collection of your personal data.You can achieve this by doing the following:When you are filling the forms on the website, make sure to check if there is a box which you can leave unchecked, if you don't want to disclose your personal information.If you have already agreed to share your information with us, feel free to contact us via email and we will be more than happy to change this for you.[name] will not lease, sell or distribute your personal information to any third parties, unless we have your permission.We might do so if the law forces us.Your personal information will be used when we need to send you promotional materials if you agree to this privacy policy.";


            returnSettingData.settingModel.TermsNCondition = "<h1>Terms and Conditions</h1><h2> Terms and Conditions('Terms')</h2>Last updated: (add date)<p>Please read these Terms and Conditions('Terms', 'Terms and Conditions') carefully before using the http://www.mywebsite.com (change this) website and the My Mobile App (change this) mobile</p>application(the 'Servic') operated by My Company(change this) ('us', we, or our).Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.<b>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</b><h4> Purchases </h4> If you wish to purchase any product or service made available through the Service(Purchase),you may be asked to supply certain information relevant to your Purchase including, without limitation, your …<h4> Subscriptions </h4>Some parts of the Service are billed on a subscription basis(Subscription(s)). You will be billed in advance on a recurring ...<h4> Content </h4>Our Service allows you to post, link, store, share and otherwise make available certain information,text, graphics, videos, or other material(Content).You are responsible for the<h4> Links To Other Web Sites </h4>Our Service may contain links to third - party web sites or services that are not owned or controlled by My Company(change this).My Company(change this) has no control over, and assumes no responsibility for, the content,privacy policies, or practices of any third party web sites or services.You further acknowledge and agree that My Company(change this) shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.<h4> Changes </h4>We reserve the right, at our sole discretion, to modify or replace these Terms at any time.If a revision is material we will try to provide at least 30(change this) days' notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.<h4> Contact Us </h4>If you have any questions about these Terms, please contact us.";
            
            //baseURL + "StaticFiles /privacypolicy.html";baseURL + "StaticFiles/termsncondition.html";
            return Ok(returnSettingData);
        }

        [HttpGet("{email}")]
        public async Task<IActionResult> GetAllNotifications(string email)
        {
            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();

            try
            {
                var result = await _userRepository.GetAllUsersAsync();
                var user = result.Where(x => x.Email == email).FirstOrDefault();

                if (user == null)
                {
                    returnResponse.Status_Code = Convert.ToInt32(NotFound().StatusCode);
                    returnResponse.Message = "User not exist";

                    return NotFound(returnResponse);
                }
                var notificationDetails = await _settingRepository.GetAllNotifications(user.Id);
                if (notificationDetails.Count() == 0)
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Notification not found";
                    return BadRequest(returnResponse);

                }
                ReturnNotificationData returnData = new ReturnNotificationData();
                returnData.Notification_Detail = new List<NotificationModel>();

                returnData.Status_Code = Convert.ToInt32(Ok().StatusCode);
                returnData.Message = "Notification Details";
                returnData.Notification_Detail = notificationDetails;

                return Ok(returnData);
            }
            catch (Exception ex)
            {
                returnResponse.Status_Code = 500;
                returnResponse.Message = ex.ToString();

                return BadRequest(returnResponse);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateGallery([FromBody] EditNotificationModel editNotificationModel)
        {
            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();
            try
            {
                var result = await _userRepository.GetAllUsersAsync();
                var user = result.Where(x => x.Email == editNotificationModel.Email).FirstOrDefault();

                if (user == null)
                {
                    returnResponse.Status_Code = Convert.ToInt32(NotFound().StatusCode);
                    returnResponse.Message = "User not exist";

                    return NotFound(returnResponse);
                }

                var notificationDetails = await _settingRepository.GetNotificationById(editNotificationModel.Id);

                if (notificationDetails.Count() == 0)
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Notification not found";
                    return BadRequest(returnResponse);
                }

                var updatedOutput = await _settingRepository.UpdateIsReadAsync(user.Id, editNotificationModel);
                if (updatedOutput == 1)
                {
                    var updatednotificationDetails = await _settingRepository.GetNotificationById(editNotificationModel.Id);

                    ReturnNotificationData returnData = new ReturnNotificationData();
                    returnData.Notification_Detail = new List<NotificationModel>();

                    returnData.Status_Code = Convert.ToInt32(Ok().StatusCode);
                    returnData.Message = "Notification updated";
                    returnData.Notification_Detail = updatednotificationDetails;

                    return Ok(returnData);
                }
                else
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Notification not updated";

                    return BadRequest(returnResponse);
                }

            }
            catch (Exception ex)
            {
                returnResponse.Status_Code = 500;
                returnResponse.Message = ex.ToString();

                return BadRequest(returnResponse);

            }
        }
    }
}
