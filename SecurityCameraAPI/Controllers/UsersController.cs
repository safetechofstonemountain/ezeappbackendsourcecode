﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SecurityCameraAPI.IRepository;
using SecurityCameraAPI.Models;
using SecurityCameraAPI.Settings;
using SecurityCameraAPI.Utils;
using System.Net;
using System.Security.Cryptography;

namespace SecurityCameraAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly AppSettings _appSettings;
        //private readonly UserManager<IdentityUser> _userManager;
        public UsersController(IUserRepository userRepository,IOptions<AppSettings> appSettings)
        {
            _userRepository = userRepository;
            _appSettings = appSettings.Value;
            //_userManager = userManager;
            //,UserManager<IdentityUser> userManager
        }

        [AllowAnonymous]
        [HttpPost("signin")]
        public IActionResult Signin([FromBody]SigninModel signinModel)
        {
            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();
            try
            {
                ReturnData returnData = new ReturnData();
                returnData.User_Detail = new UserModel();                

                var user = _userRepository.LoginAsync(signinModel.Email, signinModel.SecurityPin);

                if (user == null)
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Details are incorrect";

                    return BadRequest(returnResponse);
                    
                }
                else
                {
                    returnData.User_Detail.Id = user.Id;
                    returnData.User_Detail.Name = user.Name;
                    returnData.User_Detail.LastName = user.LastName;
                    returnData.User_Detail.Email = user.Email;
                    returnData.User_Detail.Password = user.Password;
                    returnData.User_Detail.SecurityPin = user.SecurityPin;
                    returnData.User_Detail.Token = user.Token;
                    returnData.User_Detail.TokenExpires = user.TokenExpires;

                    returnData.Status_Code = Convert.ToInt32(Ok().StatusCode);
                    returnData.Message = "User login successfully";

                    return Ok(returnData);
                }             
                                
                
            }
            catch(Exception ex)
            {
                returnResponse.Status_Code = 500;
                returnResponse.Message = ex.ToString();

                return BadRequest(returnResponse);
            }
        }

        [HttpPost("signinpin")]
        public async Task<IActionResult> SigninPin([FromBody] SigninPinModel signinPinModel)
        {
            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();
            try
            {
                var _user = await _userRepository.GetAllUsersAsync();
                var existuser = _user.Where(x => x.SecurityPin == signinPinModel.SecurityPin).FirstOrDefault();


                if (existuser == null)
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Pin is incorrect";

                    return BadRequest(returnResponse);
                }
                else
                {
                    returnResponse.Status_Code = Convert.ToInt32(Ok().StatusCode);
                    returnResponse.Message = "User Login Successfully";

                    return Ok(returnResponse);
                }


            }
            catch (Exception ex)
            {
                returnResponse.Status_Code = 500;
                returnResponse.Message = ex.ToString();

                return BadRequest(returnResponse);
            }
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAllUsers()
        {
            try
            {
                var users = await _userRepository.GetAllUsersAsync();
                return Ok(users);
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.ToString());
            }
            
        }

        [HttpPost("signup")]
        public async Task<IActionResult> AddNewUser([FromBody]SignupModel signupModel)
        {
            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();
            try
            {
                ReturnData returnData = new ReturnData();
                returnData.User_Detail = new UserModel();                

                var users = await _userRepository.GetAllUsersAsync();
                var existUser = users.Where(x => x.Email == signupModel.Email).FirstOrDefault();

                if(existUser !=null && existUser.Email == signupModel.Email)
                {
                    
                    returnData.User_Detail.Id = existUser.Id;
                    returnData.User_Detail.Name = existUser.Name;
                    returnData.User_Detail.LastName = existUser.LastName;
                    returnData.User_Detail.Email = existUser.Email;
                    returnData.User_Detail.Password = existUser.Password;
                    returnData.User_Detail.SecurityPin = existUser.SecurityPin;
                    returnData.User_Detail.Token = existUser.Token;
                    returnData.User_Detail.TokenExpires = existUser.TokenExpires;

                    returnData.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnData.Message = "User already exist";

                    return BadRequest(returnData);
                }

                var addedUser=await _userRepository.AddUserAsync(signupModel);
                
                if (addedUser.Token!="")
                {
                    returnData.User_Detail.Id = addedUser.Id;
                    returnData.User_Detail.Name = addedUser.Name;
                    returnData.User_Detail.LastName = addedUser.LastName;
                    returnData.User_Detail.Email = addedUser.Email;
                    returnData.User_Detail.Password = addedUser.Password;
                    returnData.User_Detail.SecurityPin = addedUser.SecurityPin;
                    returnData.User_Detail.Token = addedUser.Token;
                    returnData.User_Detail.TokenExpires = addedUser.TokenExpires;

                    returnData.Status_Code = Convert.ToInt32(Ok().StatusCode);
                    returnData.Message = "User added successfully";

                    return Ok(returnData);
                }
                else
                {
                    returnData.User_Detail.Id = 0;
                    returnData.User_Detail.Name = signupModel.Name;
                    returnData.User_Detail.LastName = signupModel.LastName;
                    returnData.User_Detail.Email = signupModel.Email;
                    returnData.User_Detail.Password = signupModel.Password;
                    returnData.User_Detail.SecurityPin = signupModel.SecurityPin;
                    returnData.User_Detail.Token = "";
                    returnData.User_Detail.TokenExpires = DateTime.Now;

                    returnData.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnData.Message = "User not added";

                    return BadRequest(returnData);
                }

                
                
            }
            catch(Exception ex)
            {
                returnResponse.Status_Code = 500;
                returnResponse.Message = ex.ToString();

                return BadRequest(returnResponse);
            }
        }        

        [HttpPut("changepin")]
        public async Task<IActionResult> ChangePin([FromBody]ChangePinModel changePinModel)
        {
            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();
            try
            {
                var _user = await _userRepository.GetAllUsersAsync();
                var existuser = _user.Where(x => x.Email == changePinModel.Email && x.SecurityPin == changePinModel.OldSecurityPin).FirstOrDefault();
                if (existuser == null)
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Details are incorrect";

                    return BadRequest(returnResponse);
                }

                var result=await _userRepository.ChangePinAsync(existuser.Id,changePinModel);
                if (result == 1)
                {
                    var updateduserDetails = await _userRepository.GetUserById(existuser.Id);

                    ReturnData returnData = new ReturnData();
                    returnData.User_Detail = new UserModel();
                    returnData.Status_Code= Convert.ToInt32(Ok().StatusCode);
                    returnData.Message= "Security Pin is updated";
                    returnData.User_Detail = updateduserDetails;
                    returnData.User_Detail.Token = "";
                    returnData.User_Detail.TokenExpires = DateTime.Now;

                    return Ok(returnData);
                }
                else
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Security Pin is not updated";

                    return BadRequest(returnResponse);
                }

                
            }
            catch(Exception ex)
            {
                returnResponse.Status_Code = 500;
                returnResponse.Message = ex.ToString();

                return BadRequest(returnResponse);
            }
        }

        [HttpPost("forgotpin")]
        public async Task<ActionResult> ForgotSecurityPin([FromBody] ForgotPinModel forgotPinModel)
        {
            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();
            try
            {
                var result = await _userRepository.GetAllUsersAsync();
                var user=result.Where(x=>x.Email==forgotPinModel.Email).FirstOrDefault();

                if(user==null)
                {
                    returnResponse.Status_Code = Convert.ToInt32(NotFound().StatusCode);
                    returnResponse.Message = "User not exist";
                    

                    return NotFound(returnResponse);
                }

                EmailService emailService = new EmailService(_appSettings);
                emailService.SendUserDetailViaEmail(user);

                returnResponse.Status_Code = Convert.ToInt32(Ok().StatusCode);
                returnResponse.Message = "Email sent successfully";

                return Ok(returnResponse);
            }
            catch(Exception ex)
            {
                returnResponse.Status_Code = 500;
                returnResponse.Message = ex.ToString();

                return BadRequest(returnResponse);
            }
            
        }

        [HttpPost("logout")]        
        public async Task<IActionResult> Logout([FromBody] LogoutModel token)
        {
            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();

            returnResponse.Status_Code = Convert.ToInt32(Ok().StatusCode);
            returnResponse.Message = "Logout SuccessFully";

            return Ok(returnResponse);

        }


    }
}
