﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecurityCameraAPI.IRepository;
using SecurityCameraAPI.Models;

namespace SecurityCameraAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmergencyContactsController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IEmergencyContactRepository _contactRepository;
        public EmergencyContactsController(IUserRepository userRepository, IEmergencyContactRepository contactRepository)
        {
            _userRepository = userRepository;
            _contactRepository = contactRepository;
        }

        [HttpGet("{email}")]
        public async Task<IActionResult> GetAllContactDetails(string email)
        {
            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();
            try
            {              

                var result = await _userRepository.GetAllUsersAsync();
                var user = result.Where(x => x.Email == email).FirstOrDefault();

                if (user == null)
                {
                    returnResponse.Status_Code = Convert.ToInt32(NotFound().StatusCode);
                    returnResponse.Message = "User not exist";
                    
                    return NotFound(returnResponse);
                }
                var contactDetails = await _contactRepository.GetAllContactList(user.Id);
                if (contactDetails.Count() == 0)
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Emergency Contacts not found";
                    return BadRequest(returnResponse);
                }

                ReturnContactData returnData = new ReturnContactData();
                returnData.Contact_Detail = new List<EmergencyContactModel>();

                returnData.Status_Code=Convert.ToInt32(Ok().StatusCode);
                returnData.Message = "Emergency Contact List";
                returnData.Contact_Detail=contactDetails;

                return Ok(returnData);
            }
            catch (Exception ex)
            {
                returnResponse.Status_Code = 500;
                returnResponse.Message = ex.ToString();

                return BadRequest(returnResponse);
            }

        }
        
        [HttpPost]
        public async Task<IActionResult> AddEmergencycontact([FromBody]EmergencyContactModel contactModel)
        {
            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();
            try
            {                               

                var result = await _userRepository.GetAllUsersAsync();
                var user = result.Where(x => x.Email == contactModel.Email).FirstOrDefault();
                contactModel.UserId = user.Id;

                var userEmergencyContact = await _contactRepository.GetAllContactList(user.Id);
                if(userEmergencyContact.Count()<3)
                {

                    var Id = await _contactRepository.AddEmergencyContactAsync(contactModel);
                    var contactDetails = await _contactRepository.GetContactById(Id);

                    if (contactDetails.Count() == 0)
                    {
                        returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                        returnResponse.Message = "Emergency Contacts not found";
                        return BadRequest(returnResponse);
                    }

                    ReturnContactData returnData = new ReturnContactData();
                    returnData.Contact_Detail = new List<EmergencyContactModel>();

                    returnData.Status_Code = Convert.ToInt32(Ok().StatusCode);
                    returnData.Message = "Emergency Contact Added";
                    returnData.Contact_Detail = contactDetails;

                    return Ok(returnData);
                }
                else
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "You can add only 3 Emergency Contact";
                    return BadRequest(returnResponse);
                }

            }
            catch (Exception ex)
            {
                returnResponse.Status_Code = 500;
                returnResponse.Message = ex.ToString();

                return BadRequest(returnResponse);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateContact([FromBody]EmergencyContactModel contactModel)
        {
            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();
            try
            {  
                var result = await _userRepository.GetAllUsersAsync();
                var user = result.Where(x => x.Email == contactModel.Email).FirstOrDefault();          

                if (user == null)
                {
                    returnResponse.Status_Code = Convert.ToInt32(NotFound().StatusCode);
                    returnResponse.Message = "User not exist";

                    return NotFound(returnResponse);
                }
                contactModel.UserId = user.Id;

                var contactDetails = await _contactRepository.GetContactById(contactModel.Id);

                if (contactDetails.Count() == 0)
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Emergency Contacts not found";
                    return BadRequest(returnResponse);
                }

                var updatedOutput = await _contactRepository.UpdateContactDetailAsync(contactModel);
                if (updatedOutput == 1)
                {
                    var updatedcontactDetails = await _contactRepository.GetContactById(contactModel.Id);

                    ReturnContactData returnData = new ReturnContactData();
                    returnData.Contact_Detail = new List<EmergencyContactModel>();

                    returnData.Status_Code = Convert.ToInt32(Ok().StatusCode);
                    returnData.Message = "Emergency Contact is updated";
                    returnData.Contact_Detail = updatedcontactDetails;

                    return Ok(returnData);
                }
                else
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Emergency contact is not updated";

                    return BadRequest(returnResponse);
                }

            }
            catch(Exception ex)
            {
                returnResponse.Status_Code = 500;
                returnResponse.Message = ex.ToString();

                return BadRequest(returnResponse);
                
            }
        }
    }
}
