﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SecurityCameraAPI.Helpers;
using SecurityCameraAPI.IRepository;
using SecurityCameraAPI.Models;
using SecurityCameraAPI.Settings;
using System.Collections.Specialized;
using System.Net;

namespace SecurityCameraAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GalleriesController : ControllerBase
    {
        private readonly IGalleryRepository _galleryRepository;
        private readonly IUserRepository _userRepository;
        private readonly IConfiguration _configuration;

        public GalleriesController(IGalleryRepository galleryRepository, IUserRepository userRepository, IConfiguration configuration)
        {
            _galleryRepository = galleryRepository;
            _userRepository = userRepository;
            _configuration = configuration;            
        }

        [HttpGet("{email}")]
        public async Task<IActionResult> GetAllGalleryDetails(string email)
        {
            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();

            try
            {
                var result = await _userRepository.GetAllUsersAsync();
                var user = result.Where(x => x.Email == email).FirstOrDefault();

                if (user == null)
                {
                    returnResponse.Status_Code = Convert.ToInt32(NotFound().StatusCode);
                    returnResponse.Message = "User not exist";

                    return NotFound(returnResponse);
                }
                var galleryDetails = await _galleryRepository.GetAllGalleryList(user.Id);
                if(galleryDetails.Count()  == 0)
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Images not found";
                    return BadRequest(returnResponse);
                    
                }
                ReturnGalleryData returnData = new ReturnGalleryData();
                returnData.Gallery_Detail = new List<ReturnGalleryModel>();

                returnData.Status_Code = Convert.ToInt32(Ok().StatusCode);
                returnData.Message = "Gallery Details";
                returnData.Gallery_Detail = galleryDetails;

                return Ok(returnData);                
            }
            catch (Exception ex)
            {
                returnResponse.Status_Code = 500;
                returnResponse.Message = ex.ToString();

                return BadRequest(returnResponse);
            }

        }
        //[HttpPost("{email}/{latitude}/{longitude}/{Address}/{datetime}")] string email, string latitude, string longitude, string Address, string dateTime, IFormFile files
        [HttpPost]
        public async Task<IActionResult> UploadImage([FromForm]GalleryModel galleryModel)
        {
            var baseURL = Convert.ToString(_configuration["AppSettings:BaseURL"]);

            string uploads = Path.Combine(Directory.GetCurrentDirectory(), "ImageUpload");
            //return Ok(uploads);
            if(!Directory.Exists(uploads))
            Directory.CreateDirectory(uploads);

            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();

            try
            {
                var result = await _userRepository.GetAllUsersAsync();
                var user = result.Where(x => x.Email == galleryModel.Email).FirstOrDefault();

                if (user == null)
                {
                    returnResponse.Status_Code = Convert.ToInt32(NotFound().StatusCode);
                    returnResponse.Message = "User not exist";

                    return NotFound(returnResponse);
                }
                
                var filename = galleryModel.Files.FileName;
                var baseDirectory = baseURL + "ImageUpload/" + filename;

                string filePath = Path.Combine(uploads, filename);
                using (Stream fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await galleryModel.Files.CopyToAsync(fileStream);
                }
                

                galleryModel.UserId = user.Id;
                galleryModel.ImageURL = baseDirectory;

                var Id = await _galleryRepository.AddGalleryImageAsync(galleryModel);
                var galleryDetails = await _galleryRepository.GetGalleryById(Id);
                if (galleryDetails.Count() == 0)
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Images not found";
                    return BadRequest(returnResponse);

                }
                ReturnGalleryData returnData = new ReturnGalleryData();
                returnData.Gallery_Detail = new List<ReturnGalleryModel>();

                returnData.Status_Code = Convert.ToInt32(Ok().StatusCode);
                returnData.Message = "Gallery Image Details Added";
                returnData.Gallery_Detail = galleryDetails;

                return Ok(returnData);
                
            }
            catch (Exception ex)
            {
                returnResponse.Status_Code = 500;
                returnResponse.Message = ex.ToString();

                return BadRequest(returnResponse);
            }
        }
        [HttpPut]
        public async Task<IActionResult> UpdateGallery([FromBody] EditGalleryModel editGalleryModel)
        {
            ReturnResponse returnResponse = new ReturnResponse();
            returnResponse.Blank_Details = new BlankDictionary();
            try
            {
                var result = await _userRepository.GetAllUsersAsync();
                var user = result.Where(x => x.Email == editGalleryModel.Email).FirstOrDefault();

                if (user == null)
                {
                    returnResponse.Status_Code = Convert.ToInt32(NotFound().StatusCode);
                    returnResponse.Message = "User not exist";

                    return NotFound(returnResponse);
                }                

                var galleryDetails = await _galleryRepository.GetGalleryById(editGalleryModel.Id);

                if (galleryDetails.Count() == 0)
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Gallery Details not found";
                    return BadRequest(returnResponse);
                }

                var updatedOutput = await _galleryRepository.UpdateIsFavouriteAsync(user.Id,editGalleryModel);
                if (updatedOutput == 1)
                {
                    var updatedgalleryDetails = await _galleryRepository.GetGalleryById(editGalleryModel.Id);

                    ReturnGalleryData returnData = new ReturnGalleryData();
                    returnData.Gallery_Detail = new List<ReturnGalleryModel>();

                    returnData.Status_Code = Convert.ToInt32(Ok().StatusCode);
                    returnData.Message = "Gallery Details updated";
                    returnData.Gallery_Detail = updatedgalleryDetails;

                    return Ok(returnData);
                }
                else
                {
                    returnResponse.Status_Code = Convert.ToInt32(BadRequest().StatusCode);
                    returnResponse.Message = "Gallery Details not updated";

                    return BadRequest(returnResponse);
                }

            }
            catch (Exception ex)
            {
                returnResponse.Status_Code = 500;
                returnResponse.Message = ex.ToString();

                return BadRequest(returnResponse);

            }
        }
    }
}
