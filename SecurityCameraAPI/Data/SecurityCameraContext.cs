﻿using Microsoft.EntityFrameworkCore;

namespace SecurityCameraAPI.Data
{
    public class SecurityCameraContext : DbContext
    {
        public SecurityCameraContext(DbContextOptions<SecurityCameraContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<EmergencyContact> EmergencyContacts { get; set; }
        public DbSet<Notification> Notifications { get; set; }
    }
}
