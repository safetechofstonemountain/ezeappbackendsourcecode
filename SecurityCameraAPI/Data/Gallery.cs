﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SecurityCameraAPI.Data
{
    public class Gallery
    {
        public int Id { get; set; }
        
        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }        
        public string ImageURL { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }    
        public string? Address  { get; set; }
        public DateTime ImageDateTime { get; set; }
        public bool IsFavourite { get; set; }          



    }
}
