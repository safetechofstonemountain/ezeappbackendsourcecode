﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SecurityCameraAPI.Data
{
    public class Notification
    {
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public string NotificationStatus { get; set; }
        public DateTime StatusDateTime { get; set; }
        public bool IsRead { get; set; }
    }
}
