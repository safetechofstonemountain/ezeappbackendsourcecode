﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SecurityCameraAPI.Data
{
    public class EmergencyContact
    {
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public string Name { get; set; }
        public string Relationship { get; set; }
        public string ContactNumber { get; set; }
    }
}
