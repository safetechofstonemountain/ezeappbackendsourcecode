﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using SecurityCameraAPI.IRepository;
using SecurityCameraAPI.Models;
using SecurityCameraAPI.Settings;
using System.Data;

namespace SecurityCameraAPI.Repository
{
    public class EmergencyContactRepository : IEmergencyContactRepository
    {
        private readonly ConnectionSettings _connectionSettings;
        public EmergencyContactRepository(IOptions<ConnectionSettings> connectionSettings)
        {
            _connectionSettings = connectionSettings.Value;
        }

        public async Task<IEnumerable<EmergencyContactModel>> GetAllContactList(int userId)
        {
            var contacts = new List<EmergencyContactModel>();

            SqlConnection conn = new SqlConnection(_connectionSettings.ConnectionString);

            var commandText = string.Format("SELECT ec.[Id],[UserId],ec.[Name],[Relationship],[ContactNumber],u.[Email] " +
                                          " FROM [EmergencyContacts] ec Inner Join [Users] u on u.Id=ec.UserId WHERE ec.UserId='{0}'", userId);

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = CommandType.Text;

                conn.Open();

                var dataReader = await cmd.ExecuteReaderAsync(CommandBehavior.CloseConnection);

                while (dataReader.Read())
                {
                    var contact = new EmergencyContactModel();
                    contact.Id = Convert.ToInt32(dataReader["Id"]);
                    contact.UserId = Convert.ToInt32(dataReader["UserId"]);
                    contact.Email = Convert.ToString(dataReader["Email"]);
                    contact.Name = Convert.ToString(dataReader["Name"]);
                    contact.Relationship = Convert.ToString(dataReader["Relationship"]);
                    contact.ContactNumber = Convert.ToString(dataReader["ContactNumber"]);

                    contacts.Add(contact);
                }
                dataReader.Close();
                conn.Close();
            }
            return contacts;
        }

        public async Task<IEnumerable<EmergencyContactModel>> GetContactById(int Id)
        {
            var contacts = new List<EmergencyContactModel>();

            SqlConnection conn = new SqlConnection(_connectionSettings.ConnectionString);

            var commandText = string.Format("SELECT ec.[Id],[UserId],ec.[Name],[Relationship],[ContactNumber],u.[Email] " +
                                          " FROM [EmergencyContacts] ec Inner Join [Users] u on u.Id=ec.UserId WHERE ec.Id='{0}'", Id);

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = CommandType.Text;

                conn.Open();

                var dataReader = await cmd.ExecuteReaderAsync(CommandBehavior.CloseConnection);

                while (dataReader.Read())
                {
                    var contact = new EmergencyContactModel();
                    contact.Id = Convert.ToInt32(dataReader["Id"]);
                    contact.UserId = Convert.ToInt32(dataReader["UserId"]);
                    contact.Email = Convert.ToString(dataReader["Email"]);
                    contact.Name = Convert.ToString(dataReader["Name"]);
                    contact.Relationship = Convert.ToString(dataReader["Relationship"]);
                    contact.ContactNumber = Convert.ToString(dataReader["ContactNumber"]);

                    contacts.Add(contact);
                }
                dataReader.Close();
                conn.Close();
            }
            return contacts;
        }

        public async Task<int> AddEmergencyContactAsync(EmergencyContactModel contactModel)
        {
            
            using(SqlConnection conn=new SqlConnection(_connectionSettings.ConnectionString))
            {
                conn.Open();

                SqlCommand cmd = conn.CreateCommand();
                SqlTransaction transaction;

                transaction = conn.BeginTransaction("SampleTransaction");

                cmd.Connection=conn;
                cmd.Transaction=transaction;

                try
                {
                    string sql = string.Format($"INSERT INTO [dbo].[EmergencyContacts] ([UserId],[Name],[Relationship],[ContactNumber]) VALUES ('{contactModel.UserId}','{contactModel.Name}','{contactModel.Relationship}','{contactModel.ContactNumber}')");

                    sql = sql + "Select Scope_Identity()";
                    cmd.CommandText=sql;
                    var Id = await cmd.ExecuteScalarAsync();

                    sql = string.Format($"INSERT INTO [dbo].[Notifications] ([UserId],[NotificationStatus],[StatusDateTime],[IsRead]) VALUES ('{contactModel.UserId}','{"Emergency Contact has been added"}','{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}','{false}')");
                    cmd.CommandText = sql;
                    await cmd.ExecuteNonQueryAsync();

                    transaction.Commit();

                    return Convert.ToInt32(Id);
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                conn.Close();
            }

        }
        public async Task<int> UpdateContactDetailAsync(EmergencyContactModel contactModel)
        {
            using (SqlConnection conn=new SqlConnection(_connectionSettings.ConnectionString))
            {
                conn.Open();

                SqlCommand cmd = conn.CreateCommand();
                SqlTransaction transaction;

                transaction= conn.BeginTransaction("SampleTransaction");

                cmd.Connection = conn;
                cmd.Transaction = transaction;

                try
                {
                    var sql = string.Format($"UPDATE [dbo].[EmergencyContacts] SET [Name]='{contactModel.Name}',[Relationship]='{contactModel.Relationship}',[ContactNumber]='{contactModel.ContactNumber}' WHERE ID='{contactModel.Id}' AND USERID='{contactModel.UserId}'");
                    cmd.CommandText=sql;
                    var result=await cmd.ExecuteNonQueryAsync();

                    sql = string.Format($"INSERT INTO [dbo].[Notifications] ([UserId],[NotificationStatus],[StatusDateTime],[IsRead]) VALUES ('{contactModel.UserId}','{"Emergency Contact has been modified"}','{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}','{false}')");
                    cmd.CommandText = sql;
                    await cmd.ExecuteNonQueryAsync();

                    transaction.Commit();
                    return result;
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }
    }
}
