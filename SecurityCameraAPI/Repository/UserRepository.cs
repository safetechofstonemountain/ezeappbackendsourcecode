﻿using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SecurityCameraAPI.Data;
using SecurityCameraAPI.IRepository;
using SecurityCameraAPI.Models;
using SecurityCameraAPI.Settings;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace SecurityCameraAPI.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly SecurityCameraContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ConnectionSettings _connectionSettings;
        public UserRepository(SecurityCameraContext dbContext,IMapper mapper, IConfiguration configuration, IOptions<ConnectionSettings> connectionSettings)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _configuration = configuration;
            _connectionSettings = connectionSettings.Value;
        }
        public UserModel LoginAsync(string email, int securitypin)
        {
            var _user = GetAllUsersAsync().Result;
            var user = _user.Where(x => x.Email == email && x.SecurityPin == securitypin).FirstOrDefault();

            //return null if user not found
            if (user == null)
                return null;

            //authenticate successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration["AppSettings:Secret"]);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name,user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(365),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);
            user.TokenExpires = tokenDescriptor.Expires;           

            return user;

        }
        public async Task<List<UserModel>> GetAllUsersAsync()
        {
            var records= await _dbContext.Users.ToListAsync();
            return _mapper.Map<List<UserModel>>(records);
        }
        public async Task<UserModel> AddUserAsync(SignupModel signupModel)
        {
            var usermodel = new UserModel();
            var user = new User()
            {
                Name = signupModel.Name,
                LastName = signupModel.LastName,
                Email = signupModel.Email,
                Password = signupModel.Password,
                SecurityPin = signupModel.SecurityPin
            };
            _dbContext.Users.Add(user);
            var result=await _dbContext.SaveChangesAsync();

            if (result > 0)
            {
                usermodel = _mapper.Map<UserModel>(user);

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_configuration["AppSettings:Secret"]);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Name,user.Id.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddDays(365),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                usermodel.Token = tokenHandler.WriteToken(token);
                usermodel.TokenExpires = tokenDescriptor.Expires;


            }
            
            return usermodel;
            
        }

        public async Task<int> ChangePinAsync(int userId,ChangePinModel changePinModel)
        {
            using (SqlConnection conn = new SqlConnection(_connectionSettings.ConnectionString))
            {
                conn.Open();

                SqlCommand cmd = conn.CreateCommand();
                SqlTransaction transaction;

                transaction = conn.BeginTransaction("SampleTransaction");

                cmd.Connection = conn;
                cmd.Transaction = transaction;

                try
                {
                    var sql = string.Format($"UPDATE [dbo].[Users] SET [SecurityPin]='{changePinModel.NewSecurityPin}' WHERE ID='{userId}'");
                    cmd.CommandText = sql;
                    var result = await cmd.ExecuteNonQueryAsync();

                    sql = string.Format($"INSERT INTO [dbo].[Notifications] ([UserId],[NotificationStatus],[StatusDateTime],[IsRead]) VALUES ('{userId}','{"Security Pin has been changed"}','{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}','{false}')");
                    cmd.CommandText = sql;
                    await cmd.ExecuteNonQueryAsync();

                    transaction.Commit();
                    return result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
            //int result = 0;
            //var user = await _dbContext.Users.FindAsync(userId);
            //if (user != null)
            //{
            //    user.SecurityPin = changePinModel.NewSecurityPin;

            //    result= await _dbContext.SaveChangesAsync();                
            //}
            //return result;
        }

        public async Task<UserModel> GetUserById(int Id)
        {
            var user = new UserModel();
            SqlConnection conn = new SqlConnection(_connectionSettings.ConnectionString);

            var commandText = string.Format("SELECT [Id],[Name],[LastName],[Email],[Password],[SecurityPin] " +
                                          " FROM [Users] WHERE Id='{0}'", Id);

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = CommandType.Text;

                conn.Open();

                var dataReader = await cmd.ExecuteReaderAsync(CommandBehavior.CloseConnection);

                while (dataReader.Read())
                {
                    user.Id = Convert.ToInt32(dataReader["Id"]);
                    user.Name = Convert.ToString(dataReader["Name"]);
                    user.LastName = Convert.ToString(dataReader["LastName"]);
                    user.Email = Convert.ToString(dataReader["Email"]);
                    user.Password = Convert.ToString(dataReader["Password"]);
                    user.SecurityPin = Convert.ToInt32(dataReader["SecurityPin"]);
                    
                }
                dataReader.Close();
                conn.Close();
            }
            return user;
        }

    }
}
