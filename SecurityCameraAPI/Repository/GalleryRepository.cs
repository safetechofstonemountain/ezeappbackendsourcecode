﻿using SecurityCameraAPI.Data;
using SecurityCameraAPI.Models;
using Microsoft.Data.SqlClient;
using SecurityCameraAPI.Settings;
using System.Data;
using SecurityCameraAPI.IRepository;
using Microsoft.Extensions.Options;

namespace SecurityCameraAPI.Repository
{
    public class GalleryRepository: IGalleryRepository
    {
        private readonly SecurityCameraContext _dbContext;
        private readonly ConnectionSettings _connectionSettings;
        
        public GalleryRepository(SecurityCameraContext dbContext,IOptions<ConnectionSettings> connectionSettings)
        {
            _dbContext = dbContext;
            _connectionSettings = connectionSettings.Value;            
        }

        public async Task<IEnumerable<ReturnGalleryModel>> GetAllGalleryList(int userId)
        {
            var galleries=new List<ReturnGalleryModel>();           
            
            SqlConnection conn = new SqlConnection(_connectionSettings.ConnectionString);

            var commandText=string.Format("SELECT G.[Id],[UserId],[ImageURL],[Latitude],[Longitude],[Address],[ImageDateTime],[IsFavourite],u.[Email] " +
                                          " FROM [Galleries] G Inner Join [Users] u on u.Id=G.UserId WHERE G.UserId='{0}'", userId);

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType= CommandType.Text;

                conn.Open();

                var dataReader = await cmd.ExecuteReaderAsync(CommandBehavior.CloseConnection);

                while (dataReader.Read())
                {
                    var gallery = new ReturnGalleryModel();
                    gallery.Id = Convert.ToInt32(dataReader["Id"]);
                    gallery.UserId = Convert.ToInt32(dataReader["UserId"]);
                    gallery.Email = Convert.ToString(dataReader["Email"]);
                    gallery.ImageURL = Convert.ToString(dataReader["ImageURL"]);
                    gallery.Latitude = Convert.ToDouble(dataReader["Latitude"]);
                    gallery.Longitude = Convert.ToDouble(dataReader["Longitude"]);
                    gallery.Address = Convert.ToString(dataReader["Address"]);
                    gallery.ImageDateTime = Convert.ToDateTime(dataReader["ImageDateTime"]);
                    gallery.IsFavourite = Convert.ToBoolean(dataReader["IsFavourite"]);
                    galleries.Add(gallery);
                }
                dataReader.Close();
                conn.Close();
            }
            return galleries;
        }
        public async Task<IEnumerable<ReturnGalleryModel>> GetGalleryById(int Id)
        {
            var galleries = new List<ReturnGalleryModel>();

            SqlConnection conn = new SqlConnection(_connectionSettings.ConnectionString);

            var commandText = string.Format("SELECT G.[Id],[UserId],[ImageURL],[Latitude],[Longitude],[Address],[ImageDateTime],[IsFavourite],u.[Email] " +
                                          " FROM [Galleries] G Inner Join [Users] u on u.Id=G.UserId WHERE G.Id='{0}'", Id);

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = CommandType.Text;

                conn.Open();

                var dataReader = await cmd.ExecuteReaderAsync(CommandBehavior.CloseConnection);

                while (dataReader.Read())
                {
                    var gallery = new ReturnGalleryModel();
                    gallery.Id = Convert.ToInt32(dataReader["Id"]);
                    gallery.UserId = Convert.ToInt32(dataReader["UserId"]);
                    gallery.Email = Convert.ToString(dataReader["Email"]);
                    gallery.ImageURL = Convert.ToString(dataReader["ImageURL"]);
                    gallery.Latitude = Convert.ToDouble(dataReader["Latitude"]);
                    gallery.Longitude = Convert.ToDouble(dataReader["Longitude"]);
                    gallery.Address = Convert.ToString(dataReader["Address"]);
                    gallery.ImageDateTime = Convert.ToDateTime(dataReader["ImageDateTime"]);
                    gallery.IsFavourite = Convert.ToBoolean(dataReader["IsFavourite"]);
                    galleries.Add(gallery);
                }
                dataReader.Close();
                conn.Close();
            }
            return galleries;
        }
        public async Task<int> AddGalleryImageAsync(GalleryModel galleryModel)
        {

            using (SqlConnection conn = new SqlConnection(_connectionSettings.ConnectionString))
            {
                conn.Open();

                SqlCommand cmd = conn.CreateCommand();
                SqlTransaction transaction;

                transaction = conn.BeginTransaction("SampleTransaction");

                cmd.Connection = conn;
                cmd.Transaction = transaction;

                try
                {
                    string sql = string.Format($"INSERT INTO [dbo].[Galleries] ([UserId],[ImageURL],[Latitude],[Longitude],[Address],[ImageDateTime],[IsFavourite]) VALUES ('{galleryModel.UserId}','{galleryModel.ImageURL}','{galleryModel.Latitude}','{galleryModel.Longitude}','{galleryModel.Address}','{galleryModel.ImageDateTime.ToString("yyyy-MM-dd HH:mm:ss")}','{galleryModel.IsFavourite}')");

                    sql = sql + "Select Scope_Identity()";
                    cmd.CommandText = sql;
                    var Id = await cmd.ExecuteScalarAsync();

                    sql = string.Format($"INSERT INTO [dbo].[Notifications] ([UserId],[NotificationStatus],[StatusDateTime],[IsRead]) VALUES ('{galleryModel.UserId}','{"Photos Successfully saved in gallery"}','{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}','{false}')");
                    cmd.CommandText = sql;
                    await cmd.ExecuteNonQueryAsync();

                    transaction.Commit();

                    return Convert.ToInt32(Id);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                conn.Close();
            }

        }
        public async Task<int> UpdateIsFavouriteAsync(int userId,EditGalleryModel editGalleryModel)
        {
            using (SqlConnection conn = new SqlConnection(_connectionSettings.ConnectionString))
            {
                conn.Open();

                SqlCommand cmd = conn.CreateCommand();
                SqlTransaction transaction;

                transaction = conn.BeginTransaction("SampleTransaction");

                cmd.Connection = conn;
                cmd.Transaction = transaction;

                try
                {
                    var sql = string.Format($"UPDATE [dbo].[Galleries] SET [IsFavourite]='{editGalleryModel.IsFavourite}' WHERE ID='{editGalleryModel.Id}' AND USERID='{userId}'");
                    cmd.CommandText = sql;
                    var result = await cmd.ExecuteNonQueryAsync();

                    sql = string.Format($"INSERT INTO [dbo].[Notifications] ([UserId],[NotificationStatus],[StatusDateTime],[IsRead]) VALUES ('{userId}','{"Image details updated"}','{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}','{false}')");
                    cmd.CommandText = sql;
                    await cmd.ExecuteNonQueryAsync();

                    transaction.Commit();
                    return result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }
    }
}
