﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using SecurityCameraAPI.IRepository;
using SecurityCameraAPI.Models;
using SecurityCameraAPI.Settings;
using System.Data;

namespace SecurityCameraAPI.Repository
{
    public class SettingRepository: ISettingRepository
    {
        private readonly ConnectionSettings _connectionSettings;
        public SettingRepository(IOptions<ConnectionSettings> connectionSettings)
        {
            _connectionSettings = connectionSettings.Value;
        }
        public async Task<IEnumerable<NotificationModel>> GetAllNotifications(int userId)
        {
            var notifications = new List<NotificationModel>();

            SqlConnection conn = new SqlConnection(_connectionSettings.ConnectionString);

            var commandText = string.Format("SELECT [Id],[UserId],[NotificationStatus],[StatusDateTime],[IsRead] " +
                                          " FROM [Notifications] WHERE UserId='{0}'", userId);

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = CommandType.Text;

                conn.Open();

                var dataReader = await cmd.ExecuteReaderAsync(CommandBehavior.CloseConnection);

                while (dataReader.Read())
                {
                    var notification = new NotificationModel();
                    notification.Id = Convert.ToInt32(dataReader["Id"]);
                    notification.UserId = Convert.ToInt32(dataReader["UserId"]);
                    notification.NotificationStatus = Convert.ToString(dataReader["NotificationStatus"]);
                    notification.StatusDateTime = Convert.ToDateTime(dataReader["StatusDateTime"]);
                    notification.IsRead = Convert.ToBoolean(dataReader["IsRead"]);
                    
                    notifications.Add(notification);
                }
                dataReader.Close();
                conn.Close();
            }
            return notifications;
        }
        public async Task<IEnumerable<NotificationModel>> GetNotificationById(int Id)
        {
            var notifications = new List<NotificationModel>();

            SqlConnection conn = new SqlConnection(_connectionSettings.ConnectionString);

            var commandText = string.Format("SELECT [Id],[UserId],[NotificationStatus],[StatusDateTime],[IsRead] " +
                                          " FROM [Notifications] WHERE Id='{0}'", Id);

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = CommandType.Text;

                conn.Open();

                var dataReader = await cmd.ExecuteReaderAsync(CommandBehavior.CloseConnection);

                while (dataReader.Read())
                {
                    var notification = new NotificationModel();
                    notification.Id = Convert.ToInt32(dataReader["Id"]);
                    notification.UserId = Convert.ToInt32(dataReader["UserId"]);
                    notification.NotificationStatus = Convert.ToString(dataReader["NotificationStatus"]);
                    notification.StatusDateTime = Convert.ToDateTime(dataReader["StatusDateTime"]);
                    notification.IsRead = Convert.ToBoolean(dataReader["IsRead"]);

                    notifications.Add(notification);
                }
                dataReader.Close();
                conn.Close();
            }
            return notifications;
        }

        public async Task<int> UpdateIsReadAsync(int userId, EditNotificationModel editNotificationModel)
        {
            using (SqlConnection conn = new SqlConnection(_connectionSettings.ConnectionString))
            {
                conn.Open();

                SqlCommand cmd = conn.CreateCommand();
                SqlTransaction transaction;

                transaction = conn.BeginTransaction("SampleTransaction");

                cmd.Connection = conn;
                cmd.Transaction = transaction;

                try
                {
                    var sql = string.Format($"UPDATE [dbo].[Notifications] SET [IsRead]='{editNotificationModel.IsRead}' WHERE ID='{editNotificationModel.Id}' AND USERID='{userId}'");
                    cmd.CommandText = sql;
                    var result = await cmd.ExecuteNonQueryAsync();                   

                    transaction.Commit();
                    return result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }
    }
}
