﻿using SecurityCameraAPI.Models;

namespace SecurityCameraAPI.IRepository
{
    public interface IEmergencyContactRepository
    {
        Task<IEnumerable<EmergencyContactModel>> GetAllContactList(int userId);
        Task<IEnumerable<EmergencyContactModel>> GetContactById(int Id);
        Task<int> AddEmergencyContactAsync(EmergencyContactModel contactModel);
        Task<int> UpdateContactDetailAsync(EmergencyContactModel contactModel);
    }
}
