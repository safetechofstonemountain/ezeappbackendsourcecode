﻿using SecurityCameraAPI.Models;

namespace SecurityCameraAPI.IRepository
{
    public interface IUserRepository
    {
        UserModel LoginAsync(string email, int securitypin);
        Task<List<UserModel>> GetAllUsersAsync();
        Task<UserModel> AddUserAsync(SignupModel signupModel);
        Task<int> ChangePinAsync(int userId,ChangePinModel changePinModel);
        Task<UserModel> GetUserById(int Id);


    }
}
