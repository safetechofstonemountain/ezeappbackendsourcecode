﻿using SecurityCameraAPI.Models;

namespace SecurityCameraAPI.IRepository
{
    public interface IGalleryRepository
    {
        Task<IEnumerable<ReturnGalleryModel>> GetAllGalleryList(int userId);
        Task<IEnumerable<ReturnGalleryModel>> GetGalleryById(int Id);
        Task<int> AddGalleryImageAsync(GalleryModel galleryModel);
        Task<int> UpdateIsFavouriteAsync(int userId, EditGalleryModel editGalleryModel);
    }
}
