﻿using SecurityCameraAPI.Models;

namespace SecurityCameraAPI.IRepository
{
    public interface ISettingRepository
    {
        Task<IEnumerable<NotificationModel>> GetAllNotifications(int userId);
        Task<IEnumerable<NotificationModel>> GetNotificationById(int Id);
        Task<int> UpdateIsReadAsync(int userId, EditNotificationModel editNotificationModel);
    }
}
