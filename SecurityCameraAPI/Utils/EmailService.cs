﻿using SecurityCameraAPI.Models;
using SecurityCameraAPI.Settings;
using System.Net.Mail;

namespace SecurityCameraAPI.Utils
{
    public class EmailService
    {
        private readonly AppSettings _appSettings;
        public EmailService(AppSettings appSettings)
        {
            _appSettings = appSettings;
        }

        private void SendEmail(string EmailIDList,string EmailSubject,string EmailBody)
        {
            try
            {
                using(MailMessage msg=new MailMessage())
                {
                    msg.From = new MailAddress(_appSettings.UserName);

                    msg.To.Add(EmailIDList);

                    msg.IsBodyHtml = true;
                    msg.Subject = EmailSubject;
                    msg.Body = EmailBody;                    
                    using (SmtpClient client=new SmtpClient())
                    {
                        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls|System.Net.SecurityProtocolType.Tls11|System.Net.SecurityProtocolType.Tls12;
                        client.DeliveryMethod= SmtpDeliveryMethod.Network;
                        client.Host = _appSettings.Host;
                        client.Port = Convert.ToInt32(_appSettings.Port);
                        client.UseDefaultCredentials = false;
                        client.EnableSsl = true;
                                               
                        System.Net.NetworkCredential credential = new System.Net.NetworkCredential(_appSettings.UserName, _appSettings.Password);
                        client.Credentials = credential;
                        client.Send(msg);
                    }
                }
            }
            catch(SmtpFailedRecipientException ex)
            {

            }
        }
        public void SendUserDetailViaEmail(UserModel userModel)
        {
            string EmailBody = "";
            string EmailSubject = "User detail";

            EmailBody = "Hi " + userModel.Name + " " + userModel.LastName + "<br>";

            EmailBody = EmailBody + "Here is your Security Pin detail.Login using this pin and then change your pin from change pin tab." + "<br>";


            EmailBody = EmailBody + "Security Pin:" + userModel.SecurityPin + "<br>";

            EmailBody = EmailBody + "<br>" + "Warm Regards,";
            EmailBody = EmailBody + "<br>" + "Customer Care";

            SendEmail(userModel.Email, EmailSubject, EmailBody);
        }
    }
}
