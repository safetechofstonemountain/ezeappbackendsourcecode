﻿using AutoMapper;
using SecurityCameraAPI.Data;
using SecurityCameraAPI.Models;

namespace SecurityCameraAPI.Helpers
{
    public class ApplicationMapping:Profile
    {
        public ApplicationMapping()
        {
            CreateMap<User, UserModel>().ReverseMap();
            CreateMap<Gallery,GalleryModel>().ReverseMap();
        }
    }
}
