﻿using Microsoft.Data.SqlClient;
using System.Data;

namespace SecurityCameraAPI.Helpers
{
    public class SqlHelper : ISqlHelper
    {
        public int ExecuteNonQuery(string connectionString,string commandText,CommandType commandType,params SqlParameter[] parameters)
        {
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                using(SqlCommand cmd =new SqlCommand(commandText, conn))
                {
                    cmd.CommandType = commandType;
                    cmd.Parameters.AddRange(parameters);
                    cmd.CommandTimeout = 1800;
                    conn.Open();
                    var result = cmd.ExecuteNonQuery();
                    conn.Close();
                    return result;
                }
            }
        }
    }
}
