﻿namespace SecurityCameraAPI.Helpers
{
    public interface IJsonAttribute
    {
        object TryConvert(string modelValue, Type targertType, out bool success);
    }
}
