﻿using Microsoft.Data.SqlClient;
using System.Data;

namespace SecurityCameraAPI.Helpers
{
    public interface ISqlHelper
    {
        int ExecuteNonQuery(string connectionString, string commandText, CommandType commandType, params SqlParameter[] parameters);
    }
}
